// и компонент, который хотим протестировать
import {
	shallowMount
} from '@vue/test-utils'
import Book from '@/components/Book.vue'

// Теперь монтируем компонент и у нас появляется wrapper
test('renders Book correctly', () => {
	const wrapper = shallowMount(Book, {
		propsData: {
			bookData: {
				"id": "7e5afb68-e890-4a96-a02e-88e727847517",
				"title": "NodeJS",
				'image': 'book-2.png'
			}
		}
	})
	expect(wrapper.contains('img')).toBe(true)
})
describe('Компонент Book', () => {
	// Теперь монтируем компонент и получаем wrapper
	const data = {
		bookData: {
			"id": "7e5afb68-e890-4a96-a02e-88e727847517",
			"title": "NodeJS",
			'image': 'book-2.png'
		}
	}
	const wrapper = shallowMount(Book, {
		propsData: data
	})

	it('имеет кнопку загрузки', () => {
		expect(wrapper.html()).toContain('<img src="/img/icons/download.svg">')
	})
	it('имеет кнопку чтения', () => {
		expect(wrapper.html()).toContain('<img src="/img/icons/readme.svg">')
	})
	it('Правильно рендерит ссылку и тайтл', () => {
		expect(wrapper.html()).toContain('<a href="/book/' + data.bookData.id + '">' + data.bookData.title + '</a>')
	})
})
// <div class="book-header__title">
// 	<a :href="'/book/' + bookData.id">
// 		{{ bookData.title }}
// 	</a>
// </div>