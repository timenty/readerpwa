module.exports = {
	productionSourceMap: false,

	pluginOptions: {
		proxy: {
			enabled: false,
			context: [
				'/api/**',
				'/#/api/**'
			],
			options: {
				target: 'http://reader.localhost/api/',
				// changeOrigin: true
			}
		}
	},

	pwa: {
		name: 'ReaderPWA',
		themeColor: '#74756b'
	},

	css: {
		sourceMap: true
	}
}