import Vue from 'vue'
import Router from 'vue-router'
// import axios from 'axios'
import MyBooks from './views/MyBooks.vue'
// import ReadBook from './views/ReadBook.vue'
// import Auth from './_helpers/authenticate.js'



Vue.use(Router)

// component: ReadBook,
const router = new Router({
	routes: [
		{
			path: '/auth',
			name: 'Authorization',
			meta: {
				requiresAuth: false,
				title: 'Авторизация'
			},
			component: () => import('./views/Authorization.vue')
		},
		{
			path: '/',
			name: 'mybooks',
			component: MyBooks,
			meta: {
				requiresAuth: true,
				title:'Мои книги'
			}
		},
		{
			path: '/book/:id',
			name: 'readBook',
			meta: {
				requiresAuth: true,
				title: ''
			},
			component: () => import('./views/ReadBook.vue'),
			props: (route) => ({ id: route.params.id })
		},
		{
			path: '/settings',
			name: 'Settings',
			meta:{
				requiresAuth: true,
				title: 'Настройки'
			},
			component: () => import('./views/Settings.vue')
		},
		{
			path: '/bookmarks',
			name: 'bookmarks',
			meta: {
				requiresAuth: true,
				title:'Закладки'
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import(/* webpackChunkName: "about" */ './views/Bookmarks.vue')
		}
	]
})


router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // этот путь требует авторизации, проверяем залогинен ли
    // пользователь, и если нет, перенаправляем на страницу логина
    // if (!auth.loggedIn()) {
    let isAuth = false;
    if (isAuth) {
      next({
        path: '/auth',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // всегда так или иначе нужно вызвать next()!
  }
})


export default router