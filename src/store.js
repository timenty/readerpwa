import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		books: [{
				"id": "871598a1-9fc9-406d-b01a-e5281355cd42",
				"title": 'С++ and Node.js Integration ...',
				'image': 'book-3.jpg',
				"description": "<h3>С++ and Node.js Integration  Handbook for creating Node.js C++ addons</h3><hr><p>С++ and Node.js Integration  Handbook for creating Node.js C++ addons</p>",
				"chapters": {"ch1":''}
			}, {
				"id": "7e5afb68-e890-4a96-a02e-88e727847517",
				"title": "NodeJS",
				'image': 'book-2.png'
			}, {
				"id": "f7bdddd2-7de9-43a4-af5d-4ed137007863",
				"title": "kek",
				'image': 'book-1.png'
			},
			'lol'
		],
		bookmarks: [{
				"bookId":"871598a1-9fc9-406d-b01a-e5281355cd42",
				"title": "Бинарный поиск",
				"content":"Предположим, вы ищете фамилию человека в телефонной­ книге (какая древняя технология!). Она начинается с буквы «К». Конечно, можно начать..."
			},
			{
				"bookId":"871598a1-9fc9-406d-b01a-e5281355cd42",
				"title": "Бинарный поиск",
				"content":"Предположим, вы ищете фамилию человека в телефонной­ книге (какая древняя технология!). Она начинается с буквы «К». Конечно, можно начать..."
			}
		],
		user: {
			"email": "",
			"password": ""
		},
		remember: true
	},
	mutations: {
		toggleRememberMe(state, $bool){
			state.remember = !state.remember;
			if (typeof $bool != 'undefined') {
				state.remember = $bool;
			}
		},
		saveToken(state, token){
			// console.log('saveToken : '+ token);
			state.api_token = token;
			let expireTime = 48; 
			if (!state.remember) {
				expireTime = 12; 
			}
			let expired = new Date().getTime() + ( expireTime * (60 * (60 * 1000)/*1 min*/))/*1hour*/;
			state.expired = expired;

			localStorage.setItem('auth', JSON.stringify({
				"api_token" : token ,
				"expired" : expired
			}));
		}
	},
	actions: {},
	getters: {}
})