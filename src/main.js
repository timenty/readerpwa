import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import fontReader from './assets/fonts/fontello/css/reader.css'
import normalizeCss from '../node_modules/normalize.css/normalize.css'
import style from './assets/stylus/style.styl'
import Vue2TouchEvents from 'vue2-touch-events'

// axios.defaults.headers.get['Content-Type'] = 'application/json';
// axios.defaults.headers.get['Accept'] = 'application/json';


Vue.config.productionTip = false

Vue.use(Vue2TouchEvents,{
    touchClass: '',
    tapTolerance: 10,
    swipeTolerance: 20,
    longTapTimeInterval: 400
})


new Vue({
	fontReader,
	normalizeCss,
	style,
	router,
	store,
	created(){
		let authData = JSON.parse(localStorage.getItem('auth'));
		if (authData) {
			this.$store.commit('saveToken', authData.api_token);
		}
	},
	render: h => h(App)
}).$mount('#app')