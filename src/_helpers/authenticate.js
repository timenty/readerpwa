export function loggedIn() {
    // return authorization header with basic auth credentials
    // save api token and expired date : 2 days

    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.authdata) {
        return { 'Authorization': 'Basic ' + user.authdata };
    } else {
        return {};
    }
}